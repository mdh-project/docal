# OCAL: An Abstraction Layer for Heterogeneous Programming with OpenCL and CUDA

## Introduction
This artifact contains all experiments that were described in the paper *OCAL: An Abstraction Layer for Heterogeneous Programming with OpenCL and CUDA*. 

## Software dependencies
The following dependencies have to be installed to execute the experiments:

- CUDA 9.1
- OpenCL 1.2 or higher
- Clang 3.8.1
- Python 2.7
- CMake 2.6
- gnuplot 5.0
- OpenGL (for compiling CUDA's n-body sample)
- X11 (for compiling CUDA's n-body sample)

## Experiment workflow
The reviewer is invited to perform the following steps:

1. Download the artifact archive [https://gitlab.com/mdh-project/docal/raw/master/ocal.tar.gz](https://gitlab.com/mdh-project/docal/raw/master/ocal.tar.gz) located in this repository.

2. Unpack the archive:
    
    `tar -xzf ocal.tar.gz`

3. Change into the artifact directory:
    
    `cd ocal`
    
4. Compile all experiment binaries:
    
    `./scripts/install.sh`
    
    All arguments provided to the `install.sh` script will be directly passed to CMake when building the binaries. This can be used for example, if the dependencies are not installed in the default paths or can not be found by CMake.

5. Select the number of iterations each experiment should be run; each of OCAL's experiments will be run three times (three types of buffer). For some of the experiments additional information is needed:
    - OpenCL's Multi Device Basic: Id of Intel's platform (line 3)
    - Hdr Tone Mapping: Id of Intel's platform and Id of the CPU device (line 4 and 5)
    - N-Body Simulation: Number of devices to be used (line 6)

Edit the `inputs.json` file and enter the desired information.

6. Run the experiments:
    
    `./scripts/00-run_ocal_samples.sh`
    
7. Plot the result graphs:
    
    `./scripts/graphs/graph_creator.sh`
    
    The images of the graphs are placed in `OCAL-AE/graphs`.
